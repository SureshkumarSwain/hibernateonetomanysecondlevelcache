package com.sys.beauto.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;


@Entity
@Table(name = "person")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)

//@Cache(usage = CacheConcurrencyStrategy.READ_ONLY)
public class Person implements java.io.Serializable
{
 
        /**
         * 
         */
        private static final long serialVersionUID = -9035342833723545079L;
 
        private Long pid;
 
        private Double personAge;
 
        private String personName;
 
        @SuppressWarnings("rawtypes")
		private Set accounts = new HashSet(0);
 
        public Person()
        {
        }
 
        public Person(Double personAge, String personName, Set<Account> accounts)
        {
                this.personAge = personAge;
                this.personName = personName;
                this.accounts = accounts;
        }
 
        @Id
        @GeneratedValue(strategy =GenerationType.IDENTITY)
 
        @Column(name = "pId", unique = true, nullable = false)
        public Long getPid()
        {
                return this.pid;
        }
 
        public void setPid(Long pid)
        {
                this.pid = pid;
        }
 
        @Column(name = "personAge", precision = 22, scale = 0)
        public Double getPersonAge()
        {
                return this.personAge;
        }
 
        public void setPersonAge(Double personAge)
        {
                this.personAge = personAge;
        }
 
        @Column(name = "personName")
        public String getPersonName()
        {
                return this.personName;
        }
 
        public void setPersonName(String personName)
        {
                this.personName = personName;
        }
 
        @SuppressWarnings("unchecked")
		@OneToMany(fetch = FetchType.LAZY, mappedBy = "person")
        public Set<Account> getAccounts()
        {
                return this.accounts;
        }
 
        public void setAccounts(Set<Account> accounts)
        {
                this.accounts = accounts;
        }
 
        @Override
        public String toString()
        {
                return "Person [pid=" + pid + ", personAge=" + personAge + ", personName=" + personName + "]";
        }
}
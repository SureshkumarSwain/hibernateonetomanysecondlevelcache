package com.sys.beauto.test;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.sys.beauto.entity.Account;
import com.sys.beauto.entity.Person;
import com.sys.beauto.util.HibernateUtil;

public class SLCacheTest {
	public static void main(String[] args) throws Exception {
		/*
		 * Session ses = HibernateUtil.getSession(); Person person=
		 * ses.get(Person.class, 121l); System.out.println(person);
		 * 
		 * // Level1 cache Person person1 = ses.get(Person.class, 121l);
		 * System.out.println(person1);
		 * 
		 * ses.clear();// removes obj from level1 cache // loads from level2 cache and
		 * also keeps in level1 cache Person person2 = ses.get(Person.class, 121l);
		 * System.out.println(person2); ses.clear();// removes obj from level1 cache
		 * Thread.sleep(10000);//remove obj from level2 cache becoz of idle timeout
		 * period //load from Database software and keeps in level2,level1 cache
		 * 
		 * Person person3 = ses.get(Person.class, 121l); System.out.println(person3);
		 * 
		 * 
		 */
		SessionFactory sessionFactory = getSessionFactory();

		// Level1 cache
		Session session = sessionFactory.getCurrentSession();
		session.beginTransaction();
		System.out.println(session.get(Person.class, 2l));
		Query query = session.createQuery("from Account where accountNumber =1").setCacheable(true)
				.setCacheRegion("account");
		@SuppressWarnings("unchecked")
		List<Account> personList = query.list();
		System.out.println(personList);
		session.getTransaction().commit();
		// sessionFactory.getCache().evictEntity(Person.class, 1l);

		// Level2 cache
		Session sessionNew = sessionFactory.getCurrentSession();
		sessionNew.beginTransaction();
		System.out.println(sessionNew.get(Person.class, 2l));
		Query anotherQuery = sessionNew.createQuery("from Account where accountNumber =1");
		anotherQuery.setCacheable(true).setCacheRegion("account");
		@SuppressWarnings("unchecked")
		List<Account> personListfromCache = anotherQuery.list();
		System.out.println(personListfromCache);
		sessionNew.getTransaction().commit();

		sessionFactory.close();
	}

	private static SessionFactory getSessionFactory() {
		return new Configuration().configure().buildSessionFactory();
	}

}
